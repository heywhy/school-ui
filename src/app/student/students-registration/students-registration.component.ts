import {Component, OnInit} from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import {SessionService} from '../../services/session.service';
import {HttpErrorResponse, HttpResponse} from '@angular/common/http';
import {MatSnackBar} from '@angular/material';
import {Router} from '@angular/router';
import {AuthService} from '../../auth/auth.service';
import {StudentsService} from '../../services/students.service';
import {ErrorStateMatcher} from "@angular/material/core";


@Component({
  selector: 'app-students-registration',
  templateUrl: './students-registration.component.html',
  styleUrls: ['./students-registration.component.css']
})

export class StudentsRegistrationComponent implements OnInit {

  emailFormControl = new FormControl('', [
    Validators.required,
    Validators.email,
  ]);

  matcher = new ErrorStateMatcher();
  hide = true;
  inputs = {
    name: null,
    email: null,
    phone: null,
    gender: null,
    dob: null,
    class_id: null,
    admittedAt: null,
  };
  genders: string[] = ['male', 'female'];
  classes = null;
  schoolId: null;
  currentUser = null;

  formControl = new FormControl('', [
    Validators.required,
    Validators.email,
  ]);

  constructor (protected router: Router,
               public snackBar: MatSnackBar,
               protected studentsService: StudentsService,
               protected authService: AuthService,
               protected sessionService: SessionService)  {
    this.currentUser = sessionService.getUser();
    this.schoolId  = sessionService.getActiveSchool();
  }

  register() {
    const school = this.sessionService.getActiveSchool();
    this.studentsService.registerStudent( school.id, this.inputs)
      .subscribe((response: HttpResponse<any>) => {
        console.log(response);
        if (response.status === 201) {
          this.snackBar.open('registration successful', null, {
            duration: 2000,
            horizontalPosition: 'right'
          });

          this.router.navigateByUrl('/dashboard');
        }
      }, (err: HttpErrorResponse) => {
        console.error(err);if (err.status === 500) {
          this.snackBar.open('Please re-enter form data!', null, {
            duration: 2000,
            horizontalPosition: 'right'
          });
        }
        else if (err.status === 422){
          this.snackBar.open('Please re-enter form data correctly!', null, {
            duration: 2000,
            horizontalPosition: 'right'
          });
        }

      });
  }

  ngOnInit() {

    this.sessionService.schoolChanged.subscribe(user => {
      this.currentUser = user;
      this.getClasses();
      // console.log(user);
    });
    this.getClasses();
  }

  getClasses() {
    const school = this.sessionService. getActiveSchool();
    this.classes = school && school.classes;
  }
}






// import { Component, OnInit } from '@angular/core';
// import {FormControl, Validators} from '@angular/forms';
// import {Gender, Klass} from '../../staff/staff-registration/staff-registration.component';
//
// @Component({
//   selector: 'app-students-registration',
//   templateUrl: './students-registration.component.html',
//   styleUrls: ['./students-registration.component.css']
// })
// export class StudentsRegistrationComponent implements OnInit {
//   genderControl = new FormControl('', [Validators.required]);
//   gender: Gender[] = [
//     {name: 'Male'},
//     {name: 'Female'}
//   ];
//   klassControl = new FormControl('', [Validators.required]);
//   klass: Klass[] = [
//     {name: 'Silver'},
//     {name: 'Gold'},
//     {name: 'Diamond'},
//     {name: 'Ruby'},
//     {name: 'Copper'},
//   ];
//
//
//   inputs = {
//     name: null,
//     gender: null,
//     class_id: null,
//     dob: null,
//     email: null,
//     phone: null,
//     admittedAt: null
//   };
//
//   constructor() { }
//
//   ngOnInit() {
//   }
//
// }
