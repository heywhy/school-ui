import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StudentsRegistrationComponent } from './students-registration.component';

describe('StudentsRegistrationComponent', () => {
  let component: StudentsRegistrationComponent;
  let fixture: ComponentFixture<StudentsRegistrationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StudentsRegistrationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StudentsRegistrationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
