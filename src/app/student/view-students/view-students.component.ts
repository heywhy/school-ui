import {Component, OnDestroy, OnInit} from '@angular/core';
import {SessionService} from '../../services/session.service';
import {MatSnackBar} from '@angular/material';
import {Router} from '@angular/router';
import {StudentsService} from '../../services/students.service';
import {AuthService} from '../../auth/auth.service';
import {HttpResponse} from '@angular/common/http';
import {ModalsService} from "../../modals/modals.service";

export interface StudentRecords {
  admittedAt: string;
  dob: string;
  email: string;
  gender: number;
  group: number;
  id: number;
  name: string;
  phone: string;
}


@Component({
  selector: 'app-view-students',
  templateUrl: './view-students.component.html',
  styleUrls: ['./view-students.component.css']
})
export class ViewStudentsComponent implements OnInit {
  displayedColumns: string[] = ['name', 'email', 'gender', 'dob', 'phone', 'settings'];
  dataSource = null;
  remove = null;
  classes = null;
  classId = null;
  schoolId: null;
  currentUser = null;

  constructor(protected router: Router,
              public snackBar: MatSnackBar,
              protected studentsService: StudentsService,
              protected authService: AuthService,
              protected sessionService: SessionService,
              public modalsService: ModalsService) {
    this.currentUser = sessionService.getUser();
    this.schoolId = sessionService.getActiveSchool();
  }

  ngOnInit() {

    this.sessionService.schoolChanged.subscribe(user => {
      this.currentUser = user;
      this.getClasses();
      this.dataSource = null;
      // console.log(user);
    });
    this.getClasses();
  }


  fetchStudents() {
    const school = this.sessionService.getActiveSchool();
    console.log(this.classId);
    // this.remove = this.sessionService.schoolChanged
    //   .subscribe(() => this.fetchStudents());
    // tslint:disable:no-unused-expression
    school && this.studentsService.getStudent(school.id, this.classId)
      .subscribe((data) => {
        this.dataSource = data.body;
      }, err => {
        console.error(err);
      });
  }

  getClasses() {
    const school = this.sessionService.getActiveSchool();
    this.classes = school && school.classes;
  }

  removeStudent(row) {
    this.modalsService.deleteDialog({
      header: "Remove staff",
      content: "Are you sure you want to remove staff"
    })
      .afterClosed()
      .subscribe(v => {
        if (v) {
          const school = this.sessionService.getActiveSchool();
          this.studentsService.deleteStudent(school.id, row.id).subscribe((response: HttpResponse<any>) => {
            this.snackBar.open('deleted successfully', null, {
              duration: 2000,
              horizontalPosition: 'right'
            });
            this.dataSource = (<StudentRecords[]>this.dataSource).filter(v => v.id != row.id);
            console.log();
          });
        }
      });
  }


}
