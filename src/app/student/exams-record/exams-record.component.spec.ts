import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExamsRecordComponent } from './exams-record.component';

describe('ExamsRecordComponent', () => {
  let component: ExamsRecordComponent;
  let fixture: ComponentFixture<ExamsRecordComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExamsRecordComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExamsRecordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
