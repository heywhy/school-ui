import { Component, OnInit } from '@angular/core';
import {FormControl, Validators} from '@angular/forms';

export interface Subject {
  name: string;
}
export interface Grade {
  grade: string;
}


@Component({
  selector: 'app-enter-exam-records',
  templateUrl: './enter-exam-records.component.html',
  styleUrls: ['./enter-exam-records.component.css']
})
export class EnterExamRecordsComponent implements OnInit {

  subjectControl = new FormControl('', [Validators.required]);
  subject: Subject[] = [
    {name: 'Math'},
    {name: 'English'},
    {name: 'Physics'},
    {name: 'Chemistry'},
    {name: 'Biology'},
    {name: 'Programming'},
    {name: 'Music'},

  ];

gradeControl = new FormControl('', [Validators.required]);
  grade: Grade [] = [
    {grade: 'A'},
    {grade: 'B'},
    {grade: 'C'},
    {grade: 'D'},
    {grade: 'E'},
  ];

  constructor() {}

  ngOnInit() {
  }

}
