import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EnterExamRecordsComponent } from './enter-exam-records.component';

describe('EnterExamRecordsComponent', () => {
  let component: EnterExamRecordsComponent;
  let fixture: ComponentFixture<EnterExamRecordsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EnterExamRecordsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EnterExamRecordsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
