import { Component, OnInit } from '@angular/core';


export interface PeriodicElement {
  subject: string;
  position: number;
  grade: string;
  remark: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
  {position: 1, subject: 'Mathematics', grade: 'A', remark: 'Excellent'},
  {position: 2, subject: 'English Language', grade: 'A', remark: 'Excellent'},
  {position: 3, subject: 'Physics', grade: 'B', remark: 'Very Good'},
  {position: 4, subject: 'Chemistry', grade: 'A', remark: 'Excellent'},
  {position: 5, subject: 'Biology', grade: 'A', remark: 'Excellent'},
  {position: 6, subject: 'Programming', grade: 'A', remark: 'Excellent'},
];


@Component({
  selector: 'app-view-exam-records',
  templateUrl: './view-exam-records.component.html',
  styleUrls: ['./view-exam-records.component.css']
})


export class ViewExamRecordsComponent implements OnInit {
  displayedColumns: string[] = ['position', 'subject', 'grade', 'remark'];
  dataSource = ELEMENT_DATA;


  constructor() { }

  ngOnInit() {
  }
}
