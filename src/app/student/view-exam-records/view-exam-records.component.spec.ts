import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewExamRecordsComponent } from './view-exam-records.component';

describe('ViewExamRecordsComponent', () => {
  let component: ViewExamRecordsComponent;
  let fixture: ComponentFixture<ViewExamRecordsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewExamRecordsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewExamRecordsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
