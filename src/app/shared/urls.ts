const API_URL = '/api/v1';

export const LOGIN_URL = API_URL + '/login';
export const USER_URL = API_URL + '/user';
export const REGISTER_URL = API_URL + '/register';
export const SCHOOL_URL = API_URL + '/school/add';
export const DELETE_SCHOOL_URL = API_URL + '/school/{school}';

export const REGISTER_STAFF_URL = API_URL  + '/school/{school}/staff';
export const GET_STAFF_URL = API_URL  + '/school/{school}/staffs';
export const DELETE_STAFF_URL = API_URL + '/school/{school}/staff/{staff}';

export const REGISTER_ClASS_URL = API_URL + '/school/{school}/class';
export const DELETE_ClASS_URL = API_URL + '/school/{school}/class/{class}';

export const REGISTER_STUDENT_URL = API_URL + '/school/{school}/student';
export const GET_STUDENT_URL = API_URL + '/school/{school}/students?class={class}';
export const DELETE_STUDENT_URL = API_URL + '/school/{school}/student/{student}';

export const SEND_STAFF_EMAIL = API_URL + '/school/{school}/salary/notification';
export const ADD_ADMIN = API_URL + '/school/{school}/users';
