export interface Dictionary {
  [key: string]: string|number;
}

export const rewriteUrl = (url: string, params: Dictionary) => {
  return url.replace(/\{([a-z_]+)\}/gi, (a, param) => <string>params[param]);
};
