import { Injectable, EventEmitter } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class  SessionService {

  protected user;

  protected token;

  protected activeSchool;

  protected activeSchoolChanged = new EventEmitter(true);

  constructor() {
    this.token = JSON.parse(localStorage.getItem('token'));
  }

  get isLoggedIn() {
    return this.token != null;
  }

  get schoolChanged() {
    return this.activeSchoolChanged;
  }

  setUser(user) {
    this.user = user;
  }

  getUser() {
    return this.user;
  }

  hasRole(role: 'ROOT'|'USER') {
    if (!this.user) {
      return false;
    }
    return this.user.roles.includes(role);
  }

  getToken() {
    return this.token;
  }

  setToken(token) {
    localStorage.setItem('token', JSON.stringify(token));
    this.token = token;
  }

  setActiveSchool(school) {
    const {activeSchool} = this;
    // tslint:disable
    if (!activeSchool || (activeSchool.id != school.id)) {
      this.activeSchoolChanged.emit(school);
    }
    this.activeSchool = school;
  }

  getActiveSchool() {
    return this.activeSchool;
  }
}
