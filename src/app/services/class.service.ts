import {Injectable} from '@angular/core';
import {rewriteUrl} from '../common';
import {HttpClient} from '@angular/common/http';
import {DELETE_ClASS_URL, REGISTER_ClASS_URL} from '../shared/urls';

@Injectable({
  providedIn: 'root'
})
export class ClassService {
  constructor(protected http: HttpClient) {
  }

  addClass(schoolId: number, data: { name: string }) {
    const url = rewriteUrl(REGISTER_ClASS_URL, {
      school: schoolId
    });
    return this.http.post(url, data, {observe: 'response'});
  }

  deleteClass(schoolId: number, classId: number) {
    const url = rewriteUrl(DELETE_ClASS_URL, {
      school: schoolId,
      class: classId
    });
    return this.http.delete(url, {observe: 'response'});
  }
}
