import { Injectable } from '@angular/core';
import {SCHOOL_URL, DELETE_SCHOOL_URL} from '../shared/urls';
import {HttpClient} from '@angular/common/http';
import {rewriteUrl} from '../common';

@Injectable({
  providedIn: 'root'
})
export class SchoolService {
  constructor(protected http: HttpClient) { }


  register(data: {name: string, address: string }) {
    return this.http.post(SCHOOL_URL, data, {
      observe: 'response'
    });
  }
  delete(schoolId: number) {
    const URL = rewriteUrl(DELETE_SCHOOL_URL, {
      school: schoolId,
    });
    return this.http.delete(URL,{
      observe: 'response'
    });
  }
}
