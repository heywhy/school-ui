import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { rewriteUrl } from '../common';
import {
  DELETE_STAFF_URL, DELETE_STUDENT_URL, GET_STAFF_URL, GET_STUDENT_URL, REGISTER_STAFF_URL,
  REGISTER_STUDENT_URL
} from '../shared/urls';

@Injectable({
  providedIn: 'root'
})
export class StudentsService {

  constructor(protected http: HttpClient) {
  }

  registerStudent(schoolId: number, data: {
    name: string,
    gender: number,
    class_id: number,
    dob: string,
    email: string,
    phone: string,
    admittedAt: string}) {
  const url = rewriteUrl(REGISTER_STUDENT_URL, {
    school: schoolId
  });
    return this.http.post(url, data , {
      observe: 'response'
    });
  }


  getStudent( schoolId: number, classId: number ) {
    const url = rewriteUrl(GET_STUDENT_URL, {
      school: schoolId,
      class: classId
    });
    return this.http.get(url,  {observe: 'response'});
  }

  deleteStudent( schoolId: number, studentId: number ) {
    const url = rewriteUrl(DELETE_STUDENT_URL, {
      school: schoolId,
      student: studentId
    });
    return this.http.delete(url, {observe: 'response'});
  }
}
