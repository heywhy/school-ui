import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ADD_ADMIN} from '../shared/urls';
import {rewriteUrl} from '../common';

@Injectable({
  providedIn: 'root'
})
export class AdminService {

  constructor(protected  http: HttpClient) { }

  register(school: {id: number}, inputs: any) {
    const url = rewriteUrl(ADD_ADMIN, {
      school: school.id
    });
    return this.http.post(url, inputs, {
      observe: 'response'
    });
  }
}
