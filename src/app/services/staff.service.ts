import { Injectable } from '@angular/core';
import {rewriteUrl} from '../common';
import {DELETE_STAFF_URL, GET_STAFF_URL, REGISTER_STAFF_URL} from '../shared/urls';
import {HttpClient} from '@angular/common/http';
import {observable, Observable, of, Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class StaffService {

  // protected staffs: any = of([]);
  protected staffs: any = new Subject();

  constructor(protected http: HttpClient) {
  }
  getStaff( schoolId: number ) {
    const url = rewriteUrl(GET_STAFF_URL, {
      school: schoolId
    });
    return this.http.get(url,  {observe: 'response'});
  }

  deleteStaff( schoolId: number, staffId: number ) {
    const url = rewriteUrl(DELETE_STAFF_URL, {
      school: schoolId,
      staff: staffId
    });
    return this.http.delete(url, {observe: 'response'});
  }

  registerStaff(schoolId: number, data: {
    name: string,
    email: string,
    phone: string,
    gender: number,
    dob: string,
    date_employed: string}) {
    const url = rewriteUrl(REGISTER_STAFF_URL, {
      school: schoolId
    });
    return this.http.post(url, data, {
      observe: 'response'
    });
  }

  setStaffs(staffs) {
    this.staffs.next(staffs);
  }

  getStaffs() {
    return this.staffs;
  }
}
