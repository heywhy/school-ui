import { Route } from '@angular/router';
import { BaseComponent as BaseLayout } from './layouts/base/base.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import {StudentComponent} from './student/student.component';
import {StaffsComponent} from './staff/staffs.component';
import {StudentsRegistrationComponent} from './student/students-registration/students-registration.component';
import {EnterExamRecordsComponent} from './student/enter-exam-records/enter-exam-records.component';
import {ViewExamRecordsComponent} from './student/view-exam-records/view-exam-records.component';
import {ViewStudentsComponent} from './student/view-students/view-students.component';
import { DashboardComponent } from './layouts/dashboard/dashboard.component';
import { AuthGuard } from './auth/auth.guard';
import {DashboardHomeComponent} from './dashboard-home/dashboard-home.component';
import {ClassesComponent} from './classes/classes.component';
import {SchoolsComponent} from './schools/schools.component';
import {StaffComponent} from "./staff/staff-registration/staff-registration.component";
import {ViewStaffComponent} from "./staff/view-staff/view-staff.component";

export const routes = <Route[]>[
    {
    path: '',
    component: BaseLayout,
    children: [
      {
        path: '',
        component: LoginComponent
      },
      {
        path: 'register',
        component: RegisterComponent
      },
    ]
  },
  {
    path: 'dashboard',
    canActivate: [AuthGuard],
    canActivateChild: [AuthGuard],
    component: DashboardComponent,
    children: [
      {
        path: '',
        component: DashboardHomeComponent,
      },
      {
        path: 'students',
        component: StudentComponent,
      },
      {
        path: 'classes',
        component: ClassesComponent,
      },
      {
        path: 'students/registration',
        component: StudentsRegistrationComponent,
      },
      {
        path: 'student/enter-exam-records',
        component: EnterExamRecordsComponent,
      },
      {
        path: 'students/view-exam-records',
        component: ViewExamRecordsComponent,
      },
      {
        path: 'student/view-students',
        component: ViewStudentsComponent,
      },
      {
        path: 'staff',
        component: StaffsComponent,
        children: [
          {
            path: '',
            component: ViewStaffComponent
          },
          {
            path: 'register',
            component: StaffComponent
          }
        ]
      },{
        path: 'new-administrator',
        component: SchoolsComponent,
      },
    ]
  }
 ];
