import { Component, OnInit } from '@angular/core';
import {SessionService} from '../services/session.service';
import {HttpErrorResponse, HttpResponse} from '@angular/common/http';
import {FormControl, Validators} from '@angular/forms';
import {MatSnackBar} from '@angular/material';
import {Router} from '@angular/router';
import {AdminService} from '../services/admin.service';
import { ModalsService } from '../modals/modals.service';

@Component({
  selector: 'app-schools',
  templateUrl: './schools.component.html',
  styleUrls: ['./schools.component.css']
})
export class SchoolsComponent implements OnInit {


  inputs = {
    email: null,
    name: null,
    role: null
  };

  formControl = new FormControl('', [
    Validators.required,
    Validators.email,
  ]);

  constructor(protected router: Router,
              public snackBar: MatSnackBar,
              protected adminService: AdminService,
              protected sessionService: SessionService,
              protected modalsService: ModalsService) {
  }

  register() {
    setTimeout(() => {
    }, 2000);
    this.adminService.register(this.sessionService.getActiveSchool(), this.inputs)
      .subscribe((response: any) => {
        this.modalsService.showPassword(response.password);
        this.snackBar.open('registration successful', null, {
          duration: 2000,
          horizontalPosition: 'right'
        });

        this.inputs = {
          email: null,
          name: null,
          role: null
        };
      }, (err: HttpErrorResponse) => {
        console.error(err);
      });
  }

  ngOnInit(): void {
    // this.sessionService.getActiveSchool()
  }
}
