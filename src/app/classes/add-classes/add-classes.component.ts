import { Component, OnInit } from '@angular/core';
import {SessionService} from '../../services/session.service';
import {HttpErrorResponse, HttpResponse} from '@angular/common/http';
import {FormControl, Validators} from '@angular/forms';
import {MatSnackBar} from '@angular/material';
import {Router} from '@angular/router';
import {AuthService} from '../../auth/auth.service';
import {ClassService} from '../../services/class.service';

@Component({
  selector: 'app-add-classes',
  templateUrl: './add-classes.component.html',
  styleUrls: ['./add-classes.component.css']
})
export class AddClassesComponent implements OnInit {

  inputs = {
    name: null,
  };

  formControl = new FormControl('', [
    Validators.required,
  ]);

  constructor(protected router: Router,
              public snackBar: MatSnackBar,
              protected classService: ClassService,
              protected sessionService: SessionService) {
  }

  register() {
    const school = this.sessionService.getActiveSchool()
    this.classService.addClass(school.id, this.inputs)
      .subscribe((response: HttpResponse<any>) => {
        console.log(response);
        if (response.status === 201) {
          this.snackBar.open('registration successful', null, {
            duration: 2000,
            horizontalPosition: 'right'
          });
        }
      }, (err: HttpErrorResponse) => {
        console.error(err);
      });
  }


  ngOnInit() {
  }

}
