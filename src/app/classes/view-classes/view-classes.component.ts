import {Component, OnDestroy, OnInit} from '@angular/core';
import {SessionService} from '../../services/session.service';
import {MatSnackBar} from '@angular/material';
import {StaffService} from '../../services/staff.service';
import {ClassService} from '../../services/class.service';
import {HttpResponse} from '@angular/common/http';
import {ModalsService} from "../../modals/modals.service";

export interface Classes {
  id: number;
  name: string;
}


@Component({
  selector: 'app-view-classes',
  templateUrl: './view-classes.component.html',
  styleUrls: ['./view-classes.component.css']
})
export class ViewClassesComponent implements OnInit, OnDestroy {
  displayedColumns: string[] = ['name', 'settings'];
  dataSource = null;
  remove = null;

  constructor(protected sessionService: SessionService,
              protected classService: ClassService,
              public snackBar: MatSnackBar,
              public modalService: ModalsService) {
  }

  openDialog(element){
    this.modalService.deleteDialog({header: " DeleteClasses"})
      .afterClosed()
      .subscribe(v => {
      if (v){this.removeClass(element)}})
  }

  ngOnInit() {
    this.fetchClasses();
  }

  ngOnDestroy() {
    this.remove.unsubscribe();
  }

  fetchClasses() {
    if (this.remove) {
      this.remove.unsubscribe();
    }
    const school = this.sessionService.getActiveSchool();
    this.remove = this.sessionService.schoolChanged
      .subscribe(() => this.fetchClasses());
    // tslint:disable:no-unused-expression
    this.dataSource = school.classes;
    /*
       school && this.classService.addClass(school.className).subscribe((data) => this.dataSource = data);*/
  }

  removeClass(row) {
    const school = this.sessionService.getActiveSchool();
    this.classService.deleteClass(school.id, row.id).subscribe((response: HttpResponse<any>) => {
      this.snackBar.open('deleted successful', null, {
        duration: 2000,
        horizontalPosition: 'right'
      });
      this.dataSource = (<Classes[]>this.dataSource).filter(v => v.id != row.id);
      console.log();
    });


  }
}
