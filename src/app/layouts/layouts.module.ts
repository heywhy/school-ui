import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BaseComponent } from './base/base.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import {MatSidenavModule} from '@angular/material';
import {AppModule} from '../app.module';
import {RouterModule} from '@angular/router';


@NgModule({
  imports: [
    CommonModule,
  ],
  declarations: [BaseComponent, DashboardComponent]
})
export class LayoutsModule { }
