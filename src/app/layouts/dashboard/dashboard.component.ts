import { Component, OnInit, OnDestroy } from '@angular/core';
import { SessionService } from '../../services/session.service';
import { Observable, Subscription } from 'rxjs';
import { AuthService } from '../../auth/auth.service';
import { StudentsService } from '../../services/students.service';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  currentUser = null;

  constructor(
    protected authService: AuthService,
    protected studentsService: StudentsService,
    protected sessionService: SessionService
  ) {
    sessionService.getActiveSchool(); /*
    studentsService.registerStudent(2, {})
      .subscribe(res => console.log(res), err => console.error(err));*/
  }

  ngOnInit() {
    this.authService.getUser().subscribe(user => {
      this.currentUser = user;
      this.sessionService.setUser(user);
      const {schools} = <any>user;
      if (schools) {
        this.sessionService.setActiveSchool(schools[0]);
      }
    });
  }

  setActive(school) {
    this.sessionService.setActiveSchool(school);
  }
}
