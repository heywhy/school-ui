import {Component, OnInit} from '@angular/core';
import {MatSnackBar} from '@angular/material';
import {FormControl, Validators} from '@angular/forms';
import {AuthService, AuthToken} from '../auth/auth.service';
import {HttpErrorResponse, HttpResponse} from '@angular/common/http';
import {SessionService} from '../services/session.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {


  inputs = {
    name: null,
    email: null,
    password: null
  };

  formControl = new FormControl('', [
    Validators.required,
    Validators.email,
  ]);

  constructor(protected router: Router,
              public snackBar: MatSnackBar,
              protected authService: AuthService,
              protected sessionService: SessionService) {
  }

  register() {
    this.authService.register(this.inputs)
      .subscribe((response: HttpResponse<any>) => {
        console.log(response);
        if (response.status === 201) {
          this.snackBar.open('registration successful', null, {
            duration: 2000,
            horizontalPosition: 'right'
          });

          this.router.navigateByUrl('/dashboard');
        }
      }, (err: HttpErrorResponse) => {
        console.error(err);
        // if (err.status === 422) {
        //   const message = 'Incomplete data submitted!';
        //   // const message: string = err.error.;
        //   this.snackBar.open(message, null, {
        //     duration: 2000,
        //     horizontalPosition: 'right'
        //   });
        // } else {
        //   this.snackBar.open('Unable to complete registration at the moment, please try again in few minutes!', null, {
        //     duration: 5000,
        //     horizontalPosition: 'right'
        //   });
        // }
      });
  }

  ngOnInit(): void {
  }
}


