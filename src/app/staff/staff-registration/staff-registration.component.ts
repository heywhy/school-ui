import {Component, OnInit} from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import {SessionService} from '../../services/session.service';
import {HttpErrorResponse, HttpResponse} from '@angular/common/http';
import {MatSnackBar} from '@angular/material';
import {Router} from '@angular/router';
import {SchoolService} from '../../services/school.service';
import {StaffService} from '../../services/staff.service';
import {AuthService} from '../../auth/auth.service';
import {ErrorStateMatcher} from "@angular/material/core";


@Component({
  selector: 'app-staff-registration',
  templateUrl: './staff-registration.component.html',
  styleUrls: ['./staff-registration.component.css']
})
export class StaffComponent implements OnInit {

  emailFormControl = new FormControl('', [
    Validators.required,
    Validators.email,
  ]);

  matcher = new ErrorStateMatcher();
  hide = true;

  inputs = {
    name: null,
    email: null,
    phone: null,
    gender: null,
    dob: null,
    date_employed: null,
  };

  currentStaffs = null;

  genders: string[] = ['male', 'female'];
  schoolId: null;
  currentUser = null;

  formControl = new FormControl('', [
    Validators.required,
    Validators.email,
  ]);

  constructor (protected router: Router,
              public snackBar: MatSnackBar,
              protected staffService: StaffService,
              protected authService: AuthService,
              protected sessionService: SessionService)  {
              this.currentUser = sessionService.getUser();
              this.schoolId  = sessionService.getActiveSchool();
  }

  register() {
    const school = this.sessionService.getActiveSchool();
    this.staffService.registerStaff( school.id, this.inputs)
      .subscribe((response: HttpResponse<any>) => {
        console.log(response);
        if (response.status === 201) {
          this.currentStaffs.push((response.body));
          this.staffService.setStaffs(this.currentStaffs);
          this.snackBar.open('registration successful', null, {
            duration: 2000,
            horizontalPosition: 'right'
          });
        }
      }, (err: HttpErrorResponse) => {
        console.error(err);
        if (err.status === 500) {
          this.snackBar.open('Please re-enter form data!', null, {
            duration: 2000,
            horizontalPosition: 'right'
          });
        } else if (err.status === 422) {
          this.snackBar.open('Please re-enter form data correctly!', null, {
            duration: 2000,
            horizontalPosition: 'right'
          });
        }
      });
  }

  ngOnInit() {

    this.sessionService.schoolChanged.subscribe(user => {
      this.currentUser = user;
      // console.log(user);
    });
    this.staffService.getStaffs().subscribe(v => {
      this.currentStaffs = v;
    });
  }
}
