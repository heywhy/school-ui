import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SalaryNotificationComponent } from './salary-notification.component';

describe('SalaryNotificationComponent', () => {
  let component: SalaryNotificationComponent;
  let fixture: ComponentFixture<SalaryNotificationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SalaryNotificationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SalaryNotificationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
