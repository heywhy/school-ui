import {Component, OnInit, OnDestroy, OnChanges} from '@angular/core';
import { SessionService } from '../../services/session.service';
import {StaffService} from '../../services/staff.service';
import {HttpResponse, HttpClient} from '@angular/common/http';
import {MatSnackBar} from '@angular/material';
import { rewriteUrl } from '../../common';
import { SEND_STAFF_EMAIL } from '../../shared/urls';
import {ModalsService} from "../../modals/modals.service";


export interface StaffRecords {
  id: number;
  name: string;
  email: string;
  gender: string;
  dob: string;
  phoneNumber: number;
  dateOfEmployment: string;
  option?: any;
}
@Component({
  selector: 'app-view-staff',
  templateUrl: './view-staff.component.html',
  styleUrls: ['./view-staff.component.css']
})
export class ViewStaffComponent implements OnInit, OnDestroy/*, OnChanges*/ {
  displayedColumns: string[] = ['name', 'email', 'gender', 'phoneNumber', 'dateEmployed', 'settings'];
  dataSource = null;

  remove = null;

  constructor(protected sessionService: SessionService,
              protected staffService: StaffService,
              public snackBar: MatSnackBar,
              protected httpClient: HttpClient,
              public modalsService: ModalsService) { }

  ngOnInit() {
    this.fetchStaffs();
    this.staffService.getStaffs().subscribe(v => {
      console.log('staffs: ', v);
      this.dataSource = v;
      // this.
    });
  }

  ngOnDestroy() {
    this.remove.unsubscribe();
  }

  fetchStaffs() {
    if (this.remove) {
      this.remove.unsubscribe();
    }
    const school = this.sessionService.getActiveSchool();
    this.remove = this.sessionService.schoolChanged
      .subscribe(() => this.fetchStaffs());
    // tslint:disable:no-unused-expression
    school && this.staffService.getStaff(school.id)
      .subscribe((data) => {
        console.log(data);
        this.dataSource = data.body;
        this.staffService.setStaffs(data.body);
      });
  }

  deleteDialog(){
    return this.modalsService.deleteDialog({
      header: 'Remove staff',
      // content: "Are you sure you want to remove staff"
    });
  }

  removeStaff(row) {
    this.deleteDialog()
      .afterClosed()
      .subscribe(v => {
        console.log(v);
        if (v) {
          const school = this.sessionService.getActiveSchool();
          this.staffService.deleteStaff(school.id, row.id).subscribe((response: HttpResponse<any>) => {
            this.snackBar.open('deleted successful', null, {
              duration: 2000,
              horizontalPosition: 'right'
            });
            this.dataSource = (<StaffRecords[]>this.dataSource).filter(v => v.id != row.id);
          });
        }
      })
  }

  sendMails() {
    const url = rewriteUrl(SEND_STAFF_EMAIL, {
      school: this.sessionService.getActiveSchool().id
    });
    this.httpClient.post(url, {})
      .subscribe(() => {
        this.snackBar.open('Mails successfully sent.', null, {
          duration: 2000,
          horizontalPosition: 'right'
        });
      }, err => {
        console.error(err);
        this.snackBar.open('Unable to send email at the moment!', null, {
          duration: 2000,
          horizontalPosition: 'right'
        });
      });
  }
}
