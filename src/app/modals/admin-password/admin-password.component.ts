import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { copyToClipboard } from 'src/app/utils';

export interface AdminPasswordData {
  password: string;
}

@Component({
  selector: 'app-admin-password',
  templateUrl: './admin-password.component.html',
  styleUrls: ['./admin-password.component.css']
})
export class AdminPasswordComponent {

  constructor(
    public dialogRef: MatDialogRef<AdminPasswordComponent>,
    @Inject(MAT_DIALOG_DATA) public data: AdminPasswordData) {}

  copy(): void {
    copyToClipboard(this.data.password);
    this.dialogRef.close();
  }
}
