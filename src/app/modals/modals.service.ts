import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material';
import { AdminPasswordComponent } from './admin-password/admin-password.component';
import {DeleteDialogComponent} from "./delete-dialog/delete-dialog.component";

export interface DeleteDialogData {
  header: string;
  content?: string;
}

@Injectable({
  providedIn: 'root'
})
export class ModalsService {

  constructor(protected dialog: MatDialog) { }

  showPassword(password: string) {
    this.dialog.open(AdminPasswordComponent, {
      width: '300px',
      data: {password}
    });
  }

  deleteDialog(data: DeleteDialogData) {
    return this.dialog.open(DeleteDialogComponent, {
      data,
      width: '300px',
    });
  }
}
