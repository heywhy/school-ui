import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminPasswordComponent } from './admin-password/admin-password.component';
import { MatDialogModule, MatButtonModule, MatIconModule } from '@angular/material';
import { DeleteDialogComponent } from './delete-dialog/delete-dialog.component';

@NgModule({
  imports: [
    CommonModule,
    MatIconModule,
    MatButtonModule,
    MatDialogModule
  ],
  declarations: [AdminPasswordComponent, DeleteDialogComponent],
  entryComponents: [AdminPasswordComponent, DeleteDialogComponent]
})
export class ModalsModule { }
