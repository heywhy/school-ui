export const capitalizeWords = (words) => {
	return words.split(' ')
		.map(capitalize)
		.join(' ')
}


const capitalize = (str) => {
	return str[0].toUpperCase() + str.substring(1).toLowerCase()
}
