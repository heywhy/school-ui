import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { AuthService, AuthToken } from '../auth/auth.service';
import { Router } from '@angular/router';
import { SessionService } from '../services/session.service';
import { MatSnackBar } from '@angular/material';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import {ErrorStateMatcher} from '@angular/material/core';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
  emailFormControl = new FormControl('', [
    Validators.required,
    Validators.email,
  ]);
  matcher = new ErrorStateMatcher();
  hide = true;

  inputs = {
    email: null,
    password: null
  };

  formControl = new FormControl('', [
    Validators.required,
    Validators.email,
  ]);

  constructor(
    protected router: Router,
    public snackBar: MatSnackBar,
    protected authService: AuthService,
    protected sessionService: SessionService
  ) { }

  signIn() {
    this.authService.login(this.inputs)
      .subscribe((response: HttpResponse<AuthToken>) => {
        this.sessionService.setToken(response.body);
        this.router.navigateByUrl('/dashboard');
      }, (err: HttpErrorResponse) => {
        if (err.status === 422) {
          const message = 'Incomplete data submitted!';
          // const message: string = err.error.;
          this.snackBar.open(message, null, {
            duration: 3000,
            horizontalPosition: 'right'
          });
        } else if (err.status === 400) {
          const message = 'Invalid login details';
          // const message: string = err.error.;
          this.snackBar.open(message, null, {
            duration: 10000,
            horizontalPosition: 'right'
          });
        } else {
          this.snackBar.open('Problem connecting...', null, {
            duration: 5000,
            horizontalPosition: 'right',

          });
        }
      });
  }
}
