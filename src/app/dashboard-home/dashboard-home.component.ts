import {Component, Input, OnInit} from '@angular/core';
import {SessionService} from '../services/session.service';
import { AuthService } from '../auth/auth.service';

@Component({
  selector: 'app-dashboard-home',
  templateUrl: './dashboard-home.component.html',
  styleUrls: ['./dashboard-home.component.css']
})
export class DashboardHomeComponent implements OnInit {
  remove = null;
  currentUser = null;

  constructor(
    protected authService: AuthService,
    protected sessionService: SessionService) {
  }

  ngOnInit() {
    this.authService.getUser()
      .subscribe(user => {
        this.currentUser = user;
      });
    this.remove = this.sessionService.schoolChanged
      .subscribe(this.changedSchool);
  }

  changedSchool(school) {
    console.log(`School dashboard changed ${school.id}`);
  }

}
