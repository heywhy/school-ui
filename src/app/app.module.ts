import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { BaseComponent } from './layouts/base/base.component';
import {
  MatCardModule,
  MatIconModule,
  MatButtonModule,
  MatToolbarModule,
  MatGridListModule,
  MatFormFieldModule,
  MatInputModule, MatMenuModule, MatSidenavModule, MatListModule, MatSnackBarModule
} from '@angular/material';

import {MatTableModule} from '@angular/material/table';


import {MatSelectModule} from '@angular/material';

import { routes } from './routes';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { StaffComponent } from './staff/staff-registration/staff-registration.component';
import { StudentComponent } from './student/student.component';
import { SchoolsComponent } from './schools/schools.component';
import { StaffsComponent } from './staff/staffs.component';
import { SalaryNotificationComponent } from './staff/salary-notification/salary-notification.component';
import { StudentsRegistrationComponent } from './student/students-registration/students-registration.component';
import { ViewStudentsComponent } from './student/view-students/view-students.component';
import { ViewStaffComponent } from './staff/view-staff/view-staff.component';
import { EnterExamRecordsComponent } from './student/enter-exam-records/enter-exam-records.component';
import { ViewExamRecordsComponent } from './student/view-exam-records/view-exam-records.component';
import { HttpClientModule } from '@angular/common/http';
import { DashboardComponent } from './layouts/dashboard/dashboard.component';
import httpInterceptors from './common/http-interceptors';
import {DashboardSidenavComponent} from './dashboard-sidenav/dashboard-sidenav.component';
import {DashNavbarComponent} from './dash-navbar/dash-navbar.component';
import { DashboardHomeComponent } from './dashboard-home/dashboard-home.component';
import { ClassesComponent } from './classes/classes.component';
import { ViewClassesComponent } from './classes/view-classes/view-classes.component';
import { AddClassesComponent } from './classes/add-classes/add-classes.component';
import { AddSubjectComponent } from './classes/add-subject/add-subject.component';
import { ModalsModule } from './modals/modals.module';
import { SlimLoadingBarModule } from 'ng2-slim-loading-bar';


@NgModule({
  declarations: [AppComponent,
    HomeComponent,
    DashboardSidenavComponent,
    DashNavbarComponent,
    BaseComponent, LoginComponent, RegisterComponent, StaffComponent, StudentComponent, SchoolsComponent, StaffsComponent, SalaryNotificationComponent, StudentsRegistrationComponent, ViewStudentsComponent, ViewStaffComponent, EnterExamRecordsComponent, ViewExamRecordsComponent, DashboardComponent, DashboardHomeComponent, ClassesComponent, ViewClassesComponent, AddClassesComponent, AddSubjectComponent],

  imports: [
    RouterModule,
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    MatCardModule,
    MatMenuModule,
    MatIconModule,
    MatInputModule,
    MatButtonModule,
    MatToolbarModule,
    MatGridListModule,
    MatFormFieldModule,
    BrowserAnimationsModule,
    RouterModule.forRoot(routes),
    MatSidenavModule,
    MatListModule,
    MatSelectModule,
    MatTableModule,
    HttpClientModule,
    MatSnackBarModule,
    MatMenuModule,
    ModalsModule,
    SlimLoadingBarModule,
  ],
  providers: [
    httpInterceptors
  ],
  bootstrap: [AppComponent]
})
export class AppModule {

}
