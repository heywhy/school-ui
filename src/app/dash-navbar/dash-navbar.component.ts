import {Component, Input, OnInit} from '@angular/core';
import {SessionService} from '../services/session.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-dash-navbar',
  templateUrl: './dash-navbar.component.html',
  styleUrls: ['./dash-navbar.component.css']
})
export class DashNavbarComponent implements OnInit {

  @Input()
  appEmail: string = null;

  @Input()
  appName: string = null;

  constructor(protected sessionService: SessionService,
              protected router: Router) {

  }

  logout() {
    this.sessionService.setToken(null);
    this.router.navigateByUrl('');
  }

  ngOnInit() {
  }
}
