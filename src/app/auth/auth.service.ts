import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import {LOGIN_URL, REGISTER_URL, USER_URL} from '../shared/urls';
import { Observable } from 'rxjs';

export interface AuthToken {
  token_type: string;
  access_token: string;
  expires_in: number;
  jti: string;
  scope: string;
}

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(protected http: HttpClient) { }

  login(data: {email: string, password: string}) {
    return this.http.post<AuthToken>(LOGIN_URL, data, {
      observe: 'response'
    });
  }

  getUser() {
    return this.http.get(USER_URL);
  }

  register(data: {name: string, email: string, password: string }) {
    return this.http.post(REGISTER_URL, data, {
      observe: 'response'
    });
  }
}
